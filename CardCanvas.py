# Punchcard Art
# Copyright (C) 2019 Antoine Hebert - aconit.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from PIL import Image, ImageTk
from tkinter import Canvas
from tkinter.constants import *

class CardCanvas:

    def __init__(self, tkwindow, imagepath):
        self.windowId = tkwindow
        self.PILimageId = Image.open(imagepath)
        self.tkImageId = ImageTk.PhotoImage(self.PILimageId)
        self.canvasId = Canvas(self.windowId, background='white', height=self.tkImageId.height(),
                               width=self.tkImageId.width())
        self.canvasImageId = self.canvasId.create_image(0, 0, anchor=NW, image=self.tkImageId)
        self.canvasId.pack(side=BOTTOM)
        return

    def get_size(self):
        return self.canvasId.winfo_width(), self.canvasId.winfo_height()

    def width(self):
        return self.canvasId.winfo_width()

    def height(self):
        return self.canvasId.winfo_height()
# ****************************