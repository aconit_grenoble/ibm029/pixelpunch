# Punchcard Art
# Copyright (C) 2019 Antoine Hebert - aconit.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from tkinter import filedialog
from tkinter import messagebox
from tkinter import *


from CardCanvas import *
from CardLayer import *
from CardFont import *


def colum_line_conversion(colonne, ligne):
    ligne_conv = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 12]

    return colonne + 1, ligne_conv[ligne]


# ****************************


class CardApp:
    # cardImagePath = "Punched_card.gif"
    cardImagePath = "Punched_card_small.gif"
    apptitle = "PunchCard Art"
    newfiletitle = "Sans titre"

    def __init__(self):
        # fenetre principale
        self.root = Tk()
        self.root.title(self.apptitle)
        self.root.iconbitmap('punchico.ico')
        self.CardCanvasId = CardCanvas(self.root, self.cardImagePath)

        # initialisation
        self.current_layer = CardLayer(self.CardCanvasId)
        # Barre de menu
        self.menubar = Menu(self.root)

        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Nouveau", command=self.new_card)
        self.filemenu.add_command(label="Ouvrir", command=self.open_card)
        self.filemenu.add_command(label="Enregistrer", command=self.save_card)
        self.filemenu.add_command(label="Enregistrer sous...", command=self.save_as_card)
        self.filemenu.add_command(label="Fermer", command=self.close_card)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Quitter", command=self.root.quit)

        self.menubar.add_cascade(label="Fichier", menu=self.filemenu)

        self.toolselection = StringVar()
        self.toolmenu = Menu(self.menubar, tearoff=0)
        self.toolmenu.add_radiobutton(label="Poinçon", variable=self.toolselection, value="punch",
                                      command=self.tool_select_punch)
        self.toolmenu.add_radiobutton(label="Crayon", variable=self.toolselection, value="pencil",
                                      command=self.tool_select_pencil)
        self.toolmenu.add_radiobutton(label="Texte", variable=self.toolselection, value="text",
                                      command=self.tool_select_text)
        self.toolmenu.add_radiobutton(label="Gomme", variable=self.toolselection, value="eraser",
                                      command=self.tool_select_eraser)

        self.menubar.add_cascade(label="Outils", menu=self.toolmenu)

        self.effectmenu = Menu(self.menubar, tearoff=0)
        self.effectmenu.add_command(label="Miroir", command=self.effect_mirror)
        self.effectmenu.add_command(label="Negatif", command=self.effect_negative)

        self.menubar.add_cascade(label="Effets", menu=self.effectmenu)

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="Aide", command=self.help)
        self.helpmenu.add_command(label="A propos ...", command=self.aboutmessage)

        self.menubar.add_cascade(label="Aide", menu=self.helpmenu)

        self.root.config(menu=self.menubar)

        # Toolbar Police + Chargement des polices

        self.font_list = {}
        self.font_list["Standard"] = CardFont("cardfont_standard.png")
        self.font_list["Gras"] = CardFont("cardfont_bold.png")
        self.font_list["Italic"] = CardFont("cardfont_italic.png")
        self.font_list["Demi Hauteur"] = CardFont("cardfont_half.png")
        self.font_list["Demi Hauteur"].set_spacing(1)
        self.font_list["Italic"].set_spacing(2)

        self.current_font = StringVar(self.root)
        self.current_font.set("Standard")
        self.font_listbox = OptionMenu(self.root, self.current_font, *self.font_list)

        self.textEntry = Entry(self.root)
        self.textButtonVal = Button(self.root, text='Valider', command=self.text_draw)

        # toolbar crayon et gomme
        self.toolsizeEntry = Spinbox(self.root, from_=1, to_=10)
        # test

        self.toolselection.set("punch")
        self.tool_select_punch()
        self.toolsize = 0
        self.currentfilePath = self.newfiletitle
        self.unsaved_changes = False
        self.changes_saved()

        # Start
        self.root.mainloop()
        return

    # Outils
    def tool_select_punch(self):
        self.CardCanvasId.canvasId.unbind('<B1-Motion>')
        self.CardCanvasId.canvasId.unbind('<Button-1>')
        self.textButtonVal.pack_forget()
        self.textEntry.pack_forget()
        self.font_listbox.pack_forget()
        self.toolsizeEntry.pack_forget()

        self.CardCanvasId.canvasId.bind('<Button-1>', self.punch_draw)
        return

    def tool_select_pencil(self):
        self.CardCanvasId.canvasId.unbind('<B1-Motion>')
        self.CardCanvasId.canvasId.unbind('<Button-1>')
        self.textButtonVal.pack_forget()
        self.textEntry.pack_forget()
        self.font_listbox.pack_forget()

        self.CardCanvasId.canvasId.bind('<B1-Motion>', self.pencil_draw)
        self.CardCanvasId.canvasId.bind('<Button-1>', self.pencil_draw)
        self.toolsizeEntry.pack(padx=5, side=LEFT)

        return

    def tool_select_text(self):
        self.CardCanvasId.canvasId.unbind('<B1-Motion>')
        self.CardCanvasId.canvasId.unbind('<Button-1>')
        self.toolsizeEntry.pack_forget()

        self.textEntry.pack(padx=5, side=LEFT)
        self.textButtonVal.pack(padx=5, side=LEFT)
        self.font_listbox.pack(padx=5, side=LEFT)
        return

    def tool_select_eraser(self):
        self.CardCanvasId.canvasId.unbind('<B1-Motion>')
        self.CardCanvasId.canvasId.unbind('<Button-1>')
        self.textButtonVal.pack_forget()
        self.textEntry.pack_forget()
        self.font_listbox.pack_forget()

        self.CardCanvasId.canvasId.bind('<B1-Motion>', self.eraser_draw)
        self.CardCanvasId.canvasId.bind('<Button-1>', self.eraser_draw)
        self.toolsizeEntry.pack(padx=5, side=LEFT)
        return

    def tool_event(self, event):
        x, y = event.x, event.y


    def pencil_draw(self, event):
        self.new_changes()
        x, y = event.x, event.y
        colonne, ligne = self.current_layer.hole_pix_to_pos(x, y)

        if colonne == -1 or ligne == -1:
            return

        toolsize = int(self.toolsizeEntry.get())

        # print("Col, Ligne, size " + '{},{},{}'.format(colonne, ligne, int(toolsize/2)))

        for i in range(colonne - int(toolsize / 2), colonne + int(toolsize / 2 - 0.5) + 1):
            for j in range(ligne - int(toolsize / 2), ligne + int(toolsize / 2 - 0.5) + 1):
                self.current_layer.set_hole(i, j)
                # print("i, j " + '{},{}'.format(i, j))
        return

    def eraser_draw(self, event):
        self.new_changes()
        x, y = event.x, event.y
        colonne, ligne = self.current_layer.hole_pix_to_pos(x, y)

        if colonne == -1 or ligne == -1:
            return

        toolsize = int(self.toolsizeEntry.get())

        # print("Col, Ligne, size " + '{},{},{}'.format(colonne, ligne, int(toolsize/2)))

        for i in range(colonne - int(toolsize / 2), colonne + int(toolsize / 2) + 1):
            for j in range(ligne - int(toolsize / 2), ligne + int(toolsize / 2) + 1):
                self.current_layer.del_hole(i, j)
                # print("i, j " + '{},{}'.format(i, j))
        return

    def punch_draw(self, event):
        self.new_changes()
        x, y = event.x, event.y
        # print('{},{}'.format(x, y))
        colonne, ligne = self.current_layer.hole_pix_to_pos(x, y)
        # print('{},{}'.format(colonne, ligne))
        self.current_layer.toggle_hole(colonne, ligne)
        return

    def text_draw(self):
        self.new_changes()
        text = self.textEntry.get()

        self.font_list[self.current_font.get()].print_string(self.current_layer, text, 0, 0)
        # self.cardFontId[0][0].print_string(self.current_layer, text, 0, 0)
        return

    # Effets

    def effect_mirror(self):
        self.new_changes()
        self.current_layer.mirror_vertical()
        return

    def effect_negative(self):
        self.new_changes()
        self.current_layer.negative()
        return

    # Actions du menu "File"

    def save_as_card(self):
        savefilename = filedialog.asksaveasfilename(initialdir="C:/",
                                                    title="Choisir un fichier",
                                                    filetypes=[("text files", "*.txt")])
        self.currentfilePath = savefilename
        self.export_card(self.current_layer, savefilename)
        self.changes_saved()
        return

    def save_card(self):
        if self.currentfilePath == self.newfiletitle:
            self.save_as_card()
        else:
            self.export_card(self.current_layer, self.currentfilePath)
            self.changes_saved()
        return

    def open_card(self):
        openfilename = filedialog.askopenfilename(initialdir="C:/",
                                                  title="Choisir un fichier",
                                                  filetypes=[("text files", "*.txt")])
        self.new_card()
        self.currentfilePath = openfilename
        self.decode_card(self.current_layer, openfilename)
        self.changes_saved()
        return

    def close_card(self):
        self.currentfilePath = ""
        return

    def new_card(self):
        self.currentfilePath = self.newfiletitle
        self.changes_saved()
        del self.current_layer
        self.current_layer = CardLayer(self.CardCanvasId)
        return

    def new_changes(self):
        if not self.unsaved_changes:
            self.unsaved_changes = True
            self.root.title(self.apptitle + " - " + self.currentfilePath + "*")
        return

    def changes_saved(self):
        self.unsaved_changes = False
        self.root.title(self.apptitle + " - " + self.currentfilePath)
        return

    @staticmethod
    def decode_card(layer, open_file_path):
        readfile = open(open_file_path, "r")
        splited = []
        blank_col = 0x1000

        for line in readfile:
            splited = line.split(' ')

        for col in range(min(len(splited), 80)):
            col_value = int(splited[col], 16)
            for line in range(12):
                if (col_value & 1) and (col_value != blank_col):
                    layer.set_hole(col, line)
                col_value = col_value >> 1

        readfile.close()
        return

    @staticmethod
    def export_card(layer, save_file_path):
        if save_file_path.find(".txt") != -1:
            savefile = open(save_file_path, "w")
        else:
            savefile = open(save_file_path + ".txt", "w")

        blank_col = 0x1000

        for col in range(80):
            col_value = 0
            for line in range(11, -1, -1):
                if layer.get_hole(col, line):
                    col_value += 1
                if line == 0:
                    break
                col_value = col_value << 1

            if col_value != 0:
                savefile.write('{:04x}'.format(col_value))
            else:
                savefile.write('{:04x}'.format(blank_col))
            savefile.write(" ")
        savefile.close()

        return

    # Actions du menu "Aide"
    def help(self):
        return

    @staticmethod
    def aboutmessage():
        messagebox.showinfo("A propos", "PunchCard Art \n\n"
                                        "Logiciel d'édition de carte perforée à but artistique\n"
                                        "Proposé par l'association ACONIT\n\n"
                                        "Copyright 2019 - Antoine Hebert\n\n"
                                        "aconit.org\n\n"
                                        "This program is free software: you can redistribute it and/or modify "
                                        "it under the terms of the GNU General Public License as published by "
                                        "the Free Software Foundation, either version 3 of the License, or "
                                        "(at your option) any later version.\n\n"
                                        "This program is distributed in the hope that it will be useful, "
                                        "but WITHOUT ANY WARRANTY; without even the implied warranty of "
                                        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
                                        "GNU General Public License for more details.\n\n"
                                        "You should have received a copy of the GNU General Public License "
                                        "along with this program.  If not, see <https://www.gnu.org/licenses/>"
                            )
        return


