# Punchcard Art
# Copyright (C) 2019 Antoine Hebert - aconit.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class CardLayer:
    offsetX = 3.0508  # décalage de la 1ere colonne à gauche
    spacingX = 1.178  # ecart entre chaque colonne
    widthX = 0.6779  # largeur trou
    offsetY = 9.2307  # décalage de la ligne 9 avec le bas
    spacingY = 7.75  # ecart entre chaque ligne
    heightY = 3.0769  # hauteur trou

    def __init__(self, canvasid):
        self.cardMatrix = [[False] * 12 for _ in range(80)]  # Etat du trou
        self.cardCanvasMatrix = [[False] * 12 for _ in range(80)]  # objet rectangle pour GUI
        self.cardCanvasId = canvasid

    def __del__(self):
        for colonne in range(80):
            for ligne in range(12):
                if self.cardMatrix[colonne][ligne]:
                    self.cardCanvasId.canvasId.delete(self.cardCanvasMatrix[colonne][ligne])
        return

    def hole_pos_to_pix(self, colonne, ligne):
        x = colonne * self.cardCanvasId.width() * (self.spacingX / 100) + self.cardCanvasId.width() * (
                self.offsetX / 100)
        y = ligne * self.cardCanvasId.height() * (self.spacingY / 100) + self.cardCanvasId.height() * (
                self.offsetY / 100)
        y = self.cardCanvasId.height() - y
        return x, y

    def hole_pix_to_pos(self, x, y):
        y_prop = ((self.cardCanvasId.height() - y) / self.cardCanvasId.height()) * 100
        y_prop -= (self.offsetY - self.spacingY / 2)

        x_prop = (x / self.cardCanvasId.width()) * 100
        x_prop -= (self.offsetX - self.spacingX / 2)

        if y_prop < 0:
            ligne = -1
        else:
            ligne = int(y_prop / self.spacingY)

        if ligne > 11:
            ligne = -1

        if x_prop < 0:
            colonne = -1
        else:
            colonne = int(x_prop / self.spacingX)

        if colonne > 79:
            colonne = -1

        return colonne, ligne

    def toggle_hole(self, colonne, ligne):
        if colonne < 0 or ligne < 0 or colonne > 79 or ligne > 11:
            return

        x, y = self.hole_pos_to_pix(colonne, ligne)

        if not (self.cardMatrix[colonne][ligne]):
            self.cardMatrix[colonne][ligne] = True
            self.cardCanvasMatrix[colonne][ligne] = \
                self.cardCanvasId.canvasId.create_rectangle(x, y,
                                                            x + self.cardCanvasId.width() * (self.widthX / 100),
                                                            y + self.cardCanvasId.height() * (self.heightY / 100),
                                                            fill='black')
        else:
            self.cardMatrix[colonne][ligne] = False
            self.cardCanvasId.canvasId.delete(self.cardCanvasMatrix[colonne][ligne])

        self.cardCanvasId.canvasId.pack()
        return

    def set_hole(self, colonne, ligne):
        if colonne < 0 or ligne < 0 or colonne > 79 or ligne > 11:
            return

        x, y = self.hole_pos_to_pix(colonne, ligne)

        if not (self.cardMatrix[colonne][ligne]):
            self.cardMatrix[colonne][ligne] = True
            self.cardCanvasMatrix[colonne][ligne] = \
                self.cardCanvasId.canvasId.create_rectangle(x, y,
                                                            x + self.cardCanvasId.width() * (self.widthX / 100),
                                                            y + self.cardCanvasId.height() * (self.heightY / 100),
                                                            fill='black')

        self.cardCanvasId.canvasId.pack()
        return

    def get_hole(self, colonne, ligne):
        return self.cardMatrix[colonne][ligne]

    def del_hole(self, colonne, ligne):
        if self.cardMatrix[colonne][ligne]:
            self.cardMatrix[colonne][ligne] = False
            self.cardCanvasId.canvasId.delete(self.cardCanvasMatrix[colonne][ligne])

        self.cardCanvasId.canvasId.pack()
        return

    def mirror_vertical(self):
        for colonne in range(int(80 / 2)):
            for ligne in range(12):
                if self.get_hole(colonne, ligne) != self.get_hole(79 - colonne, ligne):
                    self.toggle_hole(colonne, ligne)
                    self.toggle_hole(79 - colonne, ligne)
        return

    def negative(self):
        for colonne in range(80):
            for ligne in range(12):
                self.toggle_hole(colonne, ligne)
        return
