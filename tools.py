

class Tools :

    def __init__(self, init_layer):
        self.current_layer = init_layer

    def set_layer(self,layer):
        self.current_layer = layer

    def pencil_draw(self, event):
        self.new_changes()
        x, y = event.x, event.y
        colonne, ligne = self.current_layer.hole_pix_to_pos(x, y)

        if colonne == -1 or ligne == -1:
            return

        toolsize = int(self.toolsizeEntry.get())

        # print("Col, Ligne, size " + '{},{},{}'.format(colonne, ligne, int(toolsize/2)))

        for i in range(colonne - int(toolsize / 2), colonne + int(toolsize / 2 - 0.5) + 1):
            for j in range(ligne - int(toolsize / 2), ligne + int(toolsize / 2 - 0.5) + 1):
                self.current_layer.set_hole(i, j)
                # print("i, j " + '{},{}'.format(i, j))
        return

    def eraser_draw(self, event):
        self.new_changes()
        x, y = event.x, event.y
        colonne, ligne = self.current_layer.hole_pix_to_pos(x, y)

        if colonne == -1 or ligne == -1:
            return

        toolsize = int(self.toolsizeEntry.get())

        # print("Col, Ligne, size " + '{},{},{}'.format(colonne, ligne, int(toolsize/2)))

        for i in range(colonne - int(toolsize / 2), colonne + int(toolsize / 2) + 1):
            for j in range(ligne - int(toolsize / 2), ligne + int(toolsize / 2) + 1):
                self.current_layer.del_hole(i, j)
                # print("i, j " + '{},{}'.format(i, j))
        return

    def punch_draw(self, event):
        self.new_changes()
        x, y = event.x, event.y
        # print('{},{}'.format(x, y))
        colonne, ligne = self.current_layer.hole_pix_to_pos(x, y)
        # print('{},{}'.format(colonne, ligne))
        self.current_layer.toggle_hole(colonne, ligne)
        return

    def text_draw(self):
        self.new_changes()
        text = self.textEntry.get()

        self.font_list[self.current_font.get()].print_string(self.current_layer, text, 0, 0)
        # self.cardFontId[0][0].print_string(self.current_layer, text, 0, 0)
        return

    # Effets

    def effect_mirror(self):
        self.new_changes()
        self.current_layer.mirror_vertical()
        return

    def effect_negative(self):
        self.new_changes()
        self.current_layer.negative()
        return