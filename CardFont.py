# Punchcard Art
# Copyright (C) 2019 Antoine Hebert - aconit.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from PIL import Image

class CardFont:
    # constante gestion de la police
    charWidth = 14
    charHeight = 12
    backGround = 255, 255, 255
    gridDelimiter = 0, 0, 0
    charColor = 255, 0, 0
    charDelimiter = 0, 0, 255

    def __init__(self, font_file):
        self.PILimageId = Image.open(font_file)
        self.fontRGB = self.PILimageId.convert('RGB')
        self.spacing = 3
        if self.PILimageId.width > 207:
            self.extended = True
        else:
            self.extended = False
        return

    def get_char_coord(self, char):
        font_line = ord(char) & 0b00001111
        font_col = (ord(char) & 0b01110000) >> 4
        x = (self.charWidth + 1) * font_col
        y = (self.charHeight + 1) * font_line
        return x, y

    def get_char_size(self, char):
        cur_char_width = 0
        cur_char_height = 0
        prop_flag = False
        x, y = self.get_char_coord(char)

        for cur_char_width in range(self.charWidth):
            if self.fontRGB.getpixel((x + cur_char_width, y)) == self.charDelimiter:
                prop_flag = True
                break

        if not prop_flag:
            cur_char_width = self.charWidth

        prop_flag = False

        for cur_char_height in range(self.charHeight):
            if self.fontRGB.getpixel((x, y + cur_char_height)) == self.charDelimiter:
                prop_flag = True
                break

        if not prop_flag:
            cur_char_height = self.charHeight

        return cur_char_width, cur_char_height

    def print_char(self, layer, char, colonne, ligne):
        x, y = self.get_char_coord(char)
        cur_char_width, cur_char_height = self.get_char_size(char)

        for i in range(x, x + cur_char_width):
            for j in range(y, y + cur_char_height):
                if self.fontRGB.getpixel((i, j)) == self.charColor:
                    layer.set_hole(colonne + (i - x), ligne + (11 - (j - y)))

        return

    def print_string(self, layer, string, colonne, ligne):

        cur_col = colonne

        for char in string:
            self.print_char(layer, char, cur_col, ligne)
            cur_char_width, cur_char_height = self.get_char_size(char)
            cur_col += cur_char_width + self.spacing

        return

    def set_spacing(self, spacing):
        self.spacing = spacing
        return
